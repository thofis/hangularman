'use strict';

angular.module('hangularman', ['ui.router', 'ui.bootstrap'])
  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl as vm'
      });

    $urlRouterProvider.otherwise('/');
  })
;
