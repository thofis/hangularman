'use strict';

angular.module('hangularman')
  .controller('MainCtrl', function ($log, $scope) {
    $log.info('Hello World!');

    var MAX_MISSES = 7;

    var misses = 0;

    var vm = this;

    vm.imagenumber = 0;

    vm.searchterm;

    init();

    ///////////////////

    function init() {
      vm.searchterm = getNextSearchterm();
    }

    vm.processLetter = function (letter) {
      $log.info('letter is ' + letter);
      var result = $scope.stCtrl.processLetter(letter);
      if (result === 'hit') {
        $log.info('hit');
      } else if (result === 'won') {
        $log.info('won');
      } else {
        $log.info('miss');
        misses++;
        if (misses === MAX_MISSES) {
          // TODO game over
          $log.info("GAME OVER. You lost!");
        } else {
          vm.imagenumber++;
        }
      }
    };


    function getNextSearchterm() {
      // TODO replace with factory function later
      return "Guess What!";
    }

  });
