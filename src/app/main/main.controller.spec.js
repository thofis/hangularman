'use strict';

describe('controllers', function(){
  var scope;

  beforeEach(module('hangularman'));

  beforeEach(inject(function($rootScope) {
  	scope = $rootScope.$new();
  }));

  it('should define more than 5 awesome things', inject(function($controller) {
    expect(scope.awesomeThings).toBeUndefined();

    var vm = $controller('MainCtrl', {
      $scope: scope
  	});

    expect(vm.hello).toBe('Hello');
  }));
});
