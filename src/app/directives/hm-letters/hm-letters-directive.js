(function () {
  'use strict';

  function linkFunction($scope, el, attrs) {
    console.log($scope.letterCallback);

  }

  function controllerFunction($scope) {

    $scope.letters = [
      ['A','B','C','D','E','F','G'],
      ['H','I','J','K','L','M'],
      ['N','O','P','Q','R','S','T'],
      ['U','V','W','X','Y','Z']
    ];

    $scope.disabledButtons = [];

    $scope.onClick = function (letter, button) {
      console.log('clicked letter ' + letter);

      // notify registered callback on clicked letter
      $scope.callback({letter: letter});

      // disable this button, as each button may be clicked only once
      $scope.disabledButtons.push(letter);
    };

    $scope.isDisabled = function(letter) {
      return _.contains($scope.disabledButtons, letter);
    };

    $scope.lettersControl = {
      reset: function() {
        console.log('reset');
        $scope.disabledButtons = [];
      }
    };

  }

  function hmLetters() {
    return {
      restricted: 'E',
      templateUrl: 'app/directives/hm-letters/hm-letters-directive.html',
      replace: true,
      scope: {
        callback: '&',
        lettersControl: '='
      },
      link: linkFunction,
      controller: controllerFunction
    };
  }

  angular.module('hangularman')
    .directive('hmLetters', hmLetters);

}());
