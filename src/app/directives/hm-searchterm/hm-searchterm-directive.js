(function () {
  'use strict';

  function linkFunction($scope, el, attrs) {

    attrs.$observe('searchterm', function() {
      console.info($scope.imgPath);
      $scope.hiddenterm = hide($scope.searchterm);
    });

    function hide(searchterm) {
      var letters = 'abcdefghijklmnopqrstuvwxyz';
      var hiddenArray =  _.map(searchterm, function(ch) {
        var chl = ch.toLowerCase();
        var hiddenChar = _.some(letters, function(l) { return l === chl });
        return hiddenChar ? '_' : ch;
      });
      return hiddenArray.join('');
    }
  }

  function controllerFunction($scope) {

    $scope.searchtermControl = {
      processLetter: function (letter) {
        console.log('processLetter: ' + letter);

        var hiddenterm = '';
        var returnValue = 'miss';

        for (var i=0; i<$scope.searchterm.length; i++) {
          if (letter.toLowerCase() === $scope.searchterm.charAt(i).toLowerCase()) {
            returnValue = 'hit';
            hiddenterm = hiddenterm + $scope.searchterm.charAt(i);
          } else {
            hiddenterm = hiddenterm + $scope.hiddenterm.charAt(i);
          }
        }
        $scope.hiddenterm = hiddenterm;
        if (won()) returnValue = 'won';
        return returnValue;

        function won() {
          return $scope.searchterm === $scope.hiddenterm;
        }
      }
    };


  }

  function hmSearchterm() {
    return {
      restricted: 'E',
      templateUrl: 'app/directives/hm-searchterm/hm-searchterm-directive.html',
      replace: true,
      scope: {
        searchterm: '@',
        searchtermControl: '='
      },
      link: linkFunction,
      controller: controllerFunction
    };
  }

  angular.module('hangularman')
    .directive('hmSearchterm', hmSearchterm);

}());
