(function () {

  'use strict';


  function hmImages() {
    return {
      restrict: 'E',
      scope: {
         number: '@'
      },
      replace: true,
      templateUrl: 'app/directives/hm-images/hm-images-directive.html',
      link: linkFunction
    };
  }

  function linkFunction($scope, element, attrs) {
    var imgPathPrefix = 'app/directives/hm-images/hangman_';
    var imagePathSuffix = '.png';

    updateImgPath();

    attrs.$observe('number', function() {
      updateImgPath();
      console.info($scope.imgPath);
    });

    function updateImgPath() {
      $scope.imgPath = imgPathPrefix + attrs.number + imagePathSuffix;
      $scope.number = attrs.number;
    }
  }

  angular.module('hangularman')
    .directive('hmImages', hmImages);

})();

